# [Drone CLI](https://github.com/drone/drone-cli) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-drone-cli.asc https://packaging.gitlab.io/drone-cli/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/drone-cli drone-cli main" | sudo tee /etc/apt/sources.list.d/morph027-drone-cli.list
```
